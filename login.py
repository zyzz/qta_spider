# -*- coding:utf-8 -*-

from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt
from settings import settings
from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException, NoSuchElementException
import time


class LoginDlg(QDialog):
    def __init__(self, parent=None):
        super(LoginDlg, self).__init__(parent)

        remember = settings.get_remember_user()

        usr = QLabel("用户：")
        pwd = QLabel("密码：")
        self.usrLineEdit = QLineEdit(settings.get_user() if remember else '')
        self.pwdLineEdit = QLineEdit()
        self.pwdLineEdit.setEchoMode(QLineEdit.Password)
        self.check_box = QCheckBox('记住用户')
        self.check_box.setCheckState(Qt.Checked if remember else Qt.Unchecked)

        gridLayout = QGridLayout()
        gridLayout.addWidget(usr, 0, 0, 1, 1)
        gridLayout.addWidget(pwd, 1, 0, 1, 1)
        gridLayout.addWidget(self.usrLineEdit, 0, 1, 1, 3);
        gridLayout.addWidget(self.pwdLineEdit, 1, 1, 1, 3);
        gridLayout.addWidget(self.check_box, 2, 1, 1, 1);

        okBtn = QPushButton("确定")
        cancelBtn = QPushButton("取消")
        btnLayout = QHBoxLayout()

        btnLayout.setSpacing(60)
        btnLayout.addWidget(okBtn)
        btnLayout.addWidget(cancelBtn)

        dlgLayout = QVBoxLayout()
        dlgLayout.setContentsMargins(40, 40, 40, 40)
        dlgLayout.addLayout(gridLayout)
        dlgLayout.addStretch(40)
        dlgLayout.addLayout(btnLayout)

        self.setLayout(dlgLayout)
        okBtn.clicked.connect(self.accept)
        cancelBtn.clicked.connect(self.reject)
        self.setWindowTitle("登录")
        self.resize(300, 200)

        self.driver = None

        if remember:
            self.pwdLineEdit.setFocus()

    def accept(self):
        user_name = self.usrLineEdit.text().strip()
        password = self.pwdLineEdit.text()
        if user_name and password:
            ''''''
            params = {'userId': user_name, 'passCode': password, 'type': 'rsa', 'mobile': ''}
            headers = {'Accept': 'application/json, text/javascript, */*; q=0.01',
                       'Accept-Encoding': 'gzip, deflate',
                       'Accept-Language': 'zh-CN,zh;q=0.8',
                       'Connection': 'keep-alive',
                       'Content-Length': '57',
                       'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                       'Cookie':'QN1=05dwd1gteCYRECm4AwREAg==; SignOnDefault=liuyuly.liu; psback=""url":"http%3A%2F%2Fehr.corp.qunar.com%2Fpsc%2FHCMPRD_10%2FEMPLOYEE%2FHRMS%2Fc%2FC_SS_MENU.C_PERS_PAYSLIP.GBL%3FPORTALPARAM_PTCNAV%3DC_PERS_PAYSLIP_GBL%26EOPP.SCNode%3DHRMS%26EOPP.SCPortal%3DEMPLOYEE%26EOPP.SCName%3DADMN_%26EOPP.SCLabel%3D%26EOPP.SCPTcname%3DPT_PTPP_SCFNAV_BASEPAGE_SCR%26FolderPath%3DPORTAL_ROOT_OBJECT.PORTAL_BASE_DATA.CO_NAVIGATION_COLLECTIONS.ADMN_.ADMN_S201505280100048311847033%26IsFolder%3Dfalse%26PortalActualURL%3Dhttp%3A%2F%2Fehr.corp.qunar.com%2Fpsc%2FHCMPRD_10%2FEMPLOYEE%2FHRMS%2Fc%2FC_SS_MENU.C_PERS_PAYSLIP.GBL%26PortalContentURL%3Dhttp%3A%2F%2Fehr.corp.qunar.com%2Fpsc%2FHCMPRD_10%2FEMPLOYEE%2FHRMS%2Fc%2FC_SS_MENU.C_PERS_PAYSLIP.GBL%26PortalContentProvider%3DHRMS%26PortalCRefLabel%3D%E6%9F%A5%E7%9C%8B%E5%B7%A5%E8%B5%84%E5%8D%95%20-%20%E6%9C%88%E5%BA%A6%26PortalRegistryName%3DEMPLOYEE%26PortalServletURI%3Dhttp%3A%2F%2Fehr.corp.qunar.com%2Fpsp%2FHCMPRD_10%2F%26PortalURI%3Dhttp%3A%2F%2Fehr.corp.qunar.com%2Fpsc%2FHCMPRD_10%2F%26PortalHostNode%3DHRMS%26NoCrumbs%3Dyes%26PortalKeyStruct%3Dyes" "label":"%E6%9F%A5%E7%9C%8B%E5%B7%A5%E8%B5%84%E5%8D%95%20-%20%E6%9C%88%E5%BA%A6" "origin":"PIA""; SSORET=http%3A%2F%2Fsso.qunarman.com%3A9000%2Flogin.jsp; RSASSO=vy4iEZCmUCVlYMk2cYjeZTNWe8os6bORNM20uOB4akxb0lhMxZAkqHc9u4%2FHhbFV4xANLWYnMKo%2BfXV8CNpfWm5lNliobY2pCp9fARECgb8CeTWNyKH0IACjOJGxqiZ%2Fv0sjsHdH7zPSYg6i78TQ67VA5JNQ6kAlUfRXMbWkkrJZLwxaAHHoCaLpnm0i30Es%2Bk29ehlw5jC9g9svE6fcw%2F3ZXzmUbeWgt%2BgP65Qh1NhvdivgBQMbNU4Fu2JmeJr8xziZhlV0Vs%2Bb78ooRiCZaXfyCAvTX6J1pU40ejvo1mI4opNFQSa3aBhm0CC1fEeuJiB%2FK3irgVkQ40Mfpp3BWCh50d%2BCBvnH6YEbj22%2BlTLoNYTCibIj%2BPxOyDcJzuUzfAs7NPJwqER0RejibeMI3n9INa78FgOGd2DLkgJMPhqhC3DPmOmcDcDzprSKZuHLw%2FJ9o%2FdNo%2BWv8E0LFXiveQ%3D%3D; QUSER=liuyuly.liu',
                       'Host': 'qsso.corp.qunar.com',
                       'Origin': 'https://qsso.corp.qunar.com',
                       'Referer': 'https://qsso.corp.qunar.com/login.php?ret=http%3A%2F%2Fsso.qunarman.com%3A9000%2Flogin.jsp&ext=ret%3Dhttp%253A%252F%252Fqta.qunarman.com%253A9000%252Fbaseinfo%252Fhtml%252Fshotel%252Flist',
                       'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36',
                       'X-Requested-With': 'XMLHttpRequest'}

            self.driver = webdriver.PhantomJS(
                executable_path=r'D:\development\phantomjs-2.1.1-windows\bin\phantomjs.exe')
            self.driver.get(settings.get_login_url())
            user_id = self.driver.find_element_by_id('userId')
            pass_code = self.driver.find_element_by_id('passCode')
            user_id.send_keys(user_name)
            pass_code.send_keys(password)
            self.driver.find_element_by_id('signIn').click()
            time.sleep(5)

            #r = self.session.post(settings.get_login_url(), params, headers=headers, verify=True)
            try:
                passwd_error = self.driver.find_element_by_id('Passwd_error')
                QMessageBox.warning(self,
                                    "登录失败!",
                                    passwd_error.text,
                                    QMessageBox.Yes)
                self.pwdLineEdit.setFocus()

            except NoSuchElementException:
                self.driver.get('http://qta.qunarman.com:9000/confirm/html/allorders')
                settings.set_remember_user(True if self.check_box.checkState() == Qt.Checked else False)
                settings.set_user(user_name)
                super(LoginDlg, self).accept()

        else:
            QMessageBox.warning(self,
                                "警告",
                                "用户名或密码不能为空!",
                                QMessageBox.Yes)
            self.usrLineEdit.setFocus()

    def get_cookies(self):
        cookies = {}
        for cookie in self.driver.get_cookies():
            cookies[cookie['name']] = cookie['value']
        return cookies
