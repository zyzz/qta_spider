from distutils.core import setup
import py2exe
includes = ["sip"]
options={"py2exe":
    {   #"compressed": 1,
        #"optimize": 2,
        "includes":includes
        #"dll_excludes":["MSVCP90.dll"]
        #"bundle_files": 1
    }
}

data_files = [('.', [r'settings.ini', r'现付模板.xlsx', '预付模板.xlsx'])]

setup(
    version = '1.0.0',
    description = '',
    name = '',
    options = options,
    zipfile = None,
    windows=[{"script":"main.py"}]
    #data_files = data_files
)