# -*- coding:utf-8 -*-
from PyQt5.QtCore import QSettings
class settings:
    ini = QSettings('settings.ini', QSettings.IniFormat)

    @classmethod
    def get_remember_user(cls):
        remember = settings.ini.value('option/remember')
        if remember:
            return bool(remember)
        return False

    @classmethod
    def set_remember_user(cls, remember):
        settings.ini.beginGroup("option");
        settings.ini.setValue('remember', remember)
        settings.ini.endGroup()

    @classmethod
    def get_user(cls):
        user = settings.ini.value('option/user')
        return user or ''

    @classmethod
    def set_user(cls, user):
        settings.ini.beginGroup("option");
        settings.ini.setValue('user', user)
        settings.ini.endGroup()

    @classmethod
    def get_login_url(cls):
        url = settings.ini.value('url/login')
        return url or ''

    @classmethod
    def get_query_url(cls):
        url = settings.ini.value('url/query')
        return url or ''