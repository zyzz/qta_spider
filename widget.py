# -*- coding:utf-8 -*-
from PyQt5.QtWidgets import *
from PyQt5.QAxContainer import QAxObject
from PyQt5.QtCore import QDir, QDate, QFile
from bs4 import BeautifulSoup
from settings import settings
from ctypes import windll
import requests
import os
import csv
import json


class Widget(QWidget):
    def __init__(self, cookies):
        super().__init__()
        self.setWindowTitle('qta_spider [Bella]')
        self.cookies = cookies
        self.resize(400, 200)
        verticalLayout = QVBoxLayout()
        # verticalLayout.addLayout(self._init_login_layout())
        verticalLayout.addLayout(self._init_file_choose_layout())
        verticalLayout.addLayout(self._init_date_choose_layout())
        verticalLayout.addLayout(self._init_process_layout())
        self.setLayout(verticalLayout)
        self.query_url = settings.get_query_url()
        self.reset_state()

    def _init_login_layout(self):
        horizontalLayout = QHBoxLayout()

        user_label = QLabel('用户名：', self)
        horizontalLayout.addWidget(user_label)

        user_edit = QLineEdit(self)
        horizontalLayout.addWidget(user_edit)

        password_label = QLabel('密码：', self)
        horizontalLayout.addWidget(password_label)

        password_edit = QLineEdit(self)
        horizontalLayout.addWidget(password_edit)

        check_box = QCheckBox('记住用户', self)
        horizontalLayout.addWidget(check_box)
        return horizontalLayout

    def _init_file_choose_layout(self):
        horizontalLayout = QHBoxLayout()

        file_label = QLabel('文件选择：', self)
        horizontalLayout.addWidget(file_label)

        self.file_edit = QLineEdit()
        horizontalLayout.addWidget(self.file_edit)

        file_button = QPushButton('选择')
        file_button.pressed.connect(self.on_choose_file)
        horizontalLayout.addWidget(file_button)

        return horizontalLayout

    def _init_date_choose_layout(self):
        horizontalLayout = QHBoxLayout()

        begin_label = QLabel('开始时间：', self)
        horizontalLayout.addWidget(begin_label)

        self.begin_date = QDateEdit(QDate.currentDate(), self)
        self.begin_date.setDisplayFormat('yyyy-MM-dd')
        horizontalLayout.addWidget(self.begin_date)

        end_label = QLabel('结束时间：', self)
        horizontalLayout.addWidget(end_label)

        self.end_date = QDateEdit(QDate.currentDate(), self)
        self.end_date.setDisplayFormat('yyyy-MM-dd')
        horizontalLayout.addWidget(self.end_date)
        return horizontalLayout

    def _init_process_layout(self):
        horizontalLayout = QHBoxLayout()

        self.progress_bar = QProgressBar(self)
        horizontalLayout.addWidget(self.progress_bar)

        process_button = QPushButton('查询')
        process_button.pressed.connect(self.on_process)
        horizontalLayout.addWidget(process_button)
        return horizontalLayout

    def reset_state(self):
        self.type = '预付账单'  # 业务类型 预付账单or现付账单
        self.order_list = []  # 待查询的订单集合
        self.export_datas = []  # 查询后的数据

    def on_choose_file(self):
        # settings = QSettings('settings.ini', QSettings.IniFormat)
        # directory = settings.value('option/directory')
        directory = ''
        file_path, _ = QFileDialog.getOpenFileName(caption='待查询订单列表')
        self.file_edit.setText(file_path)

    def read_all_order(self):
        file = QFile(self.file_edit.text())
        if not file.exists():
            QMessageBox.information(None, '提示', '未找到所选的文件！', QMessageBox.Ok)
            return False

        value = self.real_excel(self.file_edit.text())

        if isinstance(value, str):
            self.order_list.append(value)
        elif isinstance(value, list):
            for row in value:
                for col in row:
                    self.order_list.append(col)

        if not self.order_list:
            QMessageBox.information(None, '提示', '未找到待查询的订单号！', QMessageBox.Ok)
            return False
        return True

    def get_params(self):
        begin_date = self.begin_date.date().toString('yyyy-MM-dd')
        end_date = self.end_date.date().toString('yyyy-MM-dd')
        params = {
            # orderNum:
            'customerName': '',
            'orderDateStart': begin_date,
            'orderDateEnd': end_date,
            'hotelName': '',
            'contactPhone': '',
            'fromDateStart': '',
            'fromDateEnd': '',
            'hotelCity': '',
            'hdsOrderNum': '',
            'toDateStart': '',
            'toDateEnd': '',
            'orderSource': '',
            'guaranteeType': -1,
            'wrapperType': 'ALL',
            'saleChannel': '',
            'orderStatus': 0,
            'payType': '',
            'directHotel': False,
            'commissionFeeType': 0,
            'auditType': 0,
            'comfirmType': '',
            'payStatus': '',
            'confirmOrderStatus': '',
            'mobVip': False,
            'cashStatus': '',
            'bizType': '',
            'prodType': '',
            'inventoryType': '',
            'invoiceInfoStatus': '',
            'ocncStatus': '',
            'supplierType': '',
            'operationType': '',
            'directSupplied': '',
            'hotelSeq': '',
            'hotelId': '',
            'supplierId': '',
            'promotionActivityId': '',
            'activityID': '',
            'complainedType': '',
            'supplierOrderNo': '',
            'vccSwipeStatus': '',
            'isConnectSource': '',
            'connectSource': 'ZHONGHUI',
            'giftId': -1,
            'roomBizType': -1,
            'packageType': -1,
            'flashType': -1,
            'blackStatus': 'all',
            'curPage': 1,
            'pageSize': 15
        }
        return params

    def get_headers(self):
        headers = {
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'zh-CN,zh;q=0.8',
            'Connection': 'keep-alive',
            'Content-Length': '741',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Host': 'qta.qunarman.com:9000',
            'Origin': 'http://qta.qunarman.com:9000',
            'Referer': 'http://qta.qunarman.com:9000/confirm/html/allorders',
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36',
            'X-Requested-With': 'XMLHttpRequest'
        }
        return headers

    def get_type(self, bsObj):
        type = bsObj.find('div', class_='tag tag2').get_text()
        return type

    def get_state(self, bsObj):
        div = bsObj.find('div', class_='m-orderbrief clrfix')
        states = div.findAll('li')
        return (states[0].strong.get_text(), states[1].strong.get_text())  # 返回订单状态和支付状态

    def get_room_information(self, bsObj):
        div = bsObj.find('div', text="住宿日期")
        div = div.find_next_sibling('div')
        nights = div.findAll('span')[2].get_text()

        div = bsObj.find('div', text="房量")
        div = div.find_next_sibling('div')
        room_num = div.strong.get_text()
        return (nights, room_num)  # 返回间夜和房量

    def get_prepay_summarys(self, bsObj):
        div = bsObj.find('div', class_='m-pricearea')
        summarys = div.findAll('div', class_='summary')

        # 预付总价-总底价-应收佣金
        return (summarys[0].dl.dd.span.em.get_text(), summarys[1].dl.dd.span.em.get_text(),
                summarys[2].dl.dd.span.em.get_text())

    def get_prepay_details(self, bsObj):
        div = bsObj.find('div', class_='m-pricearea')
        details = div.findAll('div', class_='detail')

        # 房费-退款信息-备注
        return (details[0].dl.dd.span.em.get_text(),
                details[1].dl.dd.span.span.get_text() + details[1].dl.dd.span.em.get_text(),
                details[1].dl.div.div.get_text())

    def get_cashpay_summarys(self, bsObj):
        div = bsObj.find('div', class_='m-pricearea')
        summarys = div.findAll('div', class_='summary')

        # 前台现付总额-应收佣金
        return (summarys[0].dl.dd.span.em.get_text(), summarys[1].dl.dd.span.em.get_text())

    def get_cashpay_details(self, bsObj):
        div = bsObj.find('div', class_='m-pricearea')
        details = div.findAll('div', class_='detail')

        # 房费
        return (details[0].dl.dd.span.em.get_text())

    def real_excel(self, file_name):
        excel = QAxObject('Excel.Application')
        workbooks = excel.querySubObject("WorkBooks")
        workbook = workbooks.querySubObject("Open(string)", file_name)
        worksheet = workbook.querySubObject('Worksheets(int)', 1)
        used_range = worksheet.querySubObject("UsedRange")
        value = used_range.dynamicCall('Value')
        workbook.dynamicCall('Close(bool)', False)
        excel.dynamicCall('Quit()')
        return value

    def export(self):
        file_name, _ = QFileDialog.getSaveFileName(None, "保存文件", '.', "CSV File (*.csv)")
        file_name = os.path.abspath(file_name)

        templete_file = '预付模板'
        # col = 'J'
        if self.type == '现付订单':
            templete_file = '现付模板'
            # col = 'G'

        value = self.real_excel(os.path.abspath(templete_file))
        self.export_datas.insert(0, value[0])

        with open(file_name, 'w') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerows(self.export_datas)

        excel = QAxObject('Excel.Application')
        excel.dynamicCall("SetVisible(bool)", True)
        workbooks = excel.querySubObject("WorkBooks")
        workbook = workbooks.querySubObject("Open(string)", file_name)


    def on_process(self):
        self.reset_state()
        if not self.read_all_order():
            return

        self.progress_bar.setRange(0, len(self.order_list))

        params = self.get_params()
        for index, order in enumerate(self.order_list):
            params['orderNum'] = order
            r = requests.post(self.query_url, params, cookies=self.cookies)
            ret = json.loads(r.text)
            export_datas = []
            export_datas.append(str(order))
            if ret["ret"] and ret['data']['totalCount'] > 0:
                data = ret['data']['list'][0]
                export_datas.append(data['externalOrderNo'])
                export_datas.append(data['toDateStr'])
                export_datas.append(data['orderStatus'])
                export_datas.append(data['payStatus'])
                export_datas.append(data['roomNum'])
                export_datas.append(data['dayNum'])

                total_cashback = 0
                total_price = 0
                total_basePrice = 0
                total_commission = 0

                perDay = []
                for price in data['perDayPrice']:
                    cashback = price['cashback']
                    price_amount = price['price']['amount']
                    base_price_amount = price['noDrrPrice']['amount']
                    commission_amount = price['commission']['amount']

                    perDay.append(price_amount)
                    perDay.append(base_price_amount)

                    total_cashback += cashback
                    total_price += price_amount
                    total_basePrice += base_price_amount
                    total_commission += commission_amount

                export_datas.append(total_cashback * data['roomNum'])
                export_datas.append(total_price * data['roomNum'])
                export_datas.append(total_basePrice * data['roomNum'])
                export_datas.append(total_commission * data['roomNum'])
                export_datas += perDay

                self.progress_bar.setValue(index + 1)
            self.export_datas.append(export_datas)
        self.export()
